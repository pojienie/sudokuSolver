{- |
    this module defines App data type
-}
module App
    ( App
    , App.draw
    , App.handleEvent
    , App.attrMap
    ) where
import App.Common (AppInput)
import App.State (GameState(..))
import App.State.BoardInput as BI
import App.State.ShowAnswer as SA
import Data.Default (Default, def)
import qualified Brick as B
import qualified Graphics.Vty as V

-- | app data type
data App = App
    { gameState :: [GameState] -- ^ game state stack
    }

instance Default App where
    def = App [GSBoardInput def]

-- | draw function for brick
draw :: App -> [B.Widget Int]
draw app = case gameState app of
    [] -> [B.str "shutting down"]
    ((GSBoardInput bi):_) -> BI.draw bi
    ((GSShowAnswer sa):_) -> SA.draw sa

-- | handle event for app
handleEvent :: App -> AppInput -> B.EventM Int (B.Next App)
handleEvent ap input =
    case gameState ap of
      [] -> B.halt ap
      (GSBoardInput bi:r) -> B.continue $ App $ BI.handleEvent bi input ++ r
      (GSShowAnswer sa:r) -> B.continue $ App $ SA.handleEvent sa input ++ r

-- | attribute map for brick
attrMap :: App -> B.AttrMap
attrMap app =
    case gameState app of
      [] -> B.attrMap V.defAttr []
      (GSBoardInput bi:_) -> BI.attrMap bi
      (GSShowAnswer sa:_) -> SA.attrMap sa
