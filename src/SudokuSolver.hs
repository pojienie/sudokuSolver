{- |
    this module provides solve method that solves a sudoku board
-}
module SudokuSolver
    ( solve
    ) where
import qualified Data.Board as B

-- | generate guess board state
genGuess :: (Int, Int) -> B.Board -> [B.Board]
genGuess p b = map (\v -> B.set p v b) [1..9]

-- | solve a given board state, output all possible solution
solve :: B.Board -> [B.Board]
solve b' = solve' b' B.possibleBoardPosition
  where
    solve' b []
        | B.validBoard b = [b]
        | otherwise = []
    solve' b (p:ps)
        | B.get p b /= 0 = solve' b ps
        | otherwise = concat $ map (flip solve' ps) (filter B.validBoard (genGuess p b))
