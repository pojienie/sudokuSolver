{- |
    this module defines double direction list

    this mouule is intended to be imported qualified
-}
module Data.DoubleDirectionList
    ( DoubleDirectionList
    , empty
    , fromList
    , get
    , left
    , right
    ) where

-- | double direction list
data DoubleDirectionList a = DoubleDirectionList
    { _left :: [a]  -- ^ what's on left
    , _right :: [a] -- ^ what's on right
    }

-- | construct a empty double direction list
empty :: DoubleDirectionList a
empty = DoubleDirectionList [] []

-- | move the list to the left
left :: DoubleDirectionList a -> DoubleDirectionList a
left o@(DoubleDirectionList [] _) = o
left (DoubleDirectionList (l:ls) r) = DoubleDirectionList ls (l:r)

-- | move the list to the right
right :: DoubleDirectionList a -> DoubleDirectionList a
right o@(DoubleDirectionList _ []) = o
right (DoubleDirectionList l (r:rs)) = DoubleDirectionList (r:l) rs

-- | construct double direction list from regular list
fromList :: [a] -> DoubleDirectionList a
fromList = DoubleDirectionList []

-- | get the focused data from double direction list
get :: DoubleDirectionList a -> Maybe a
get (DoubleDirectionList l r) =
    case r of
      (x:_) -> Just x
      _     -> case l of
                 (x':_) -> Just x'
                 _      -> Nothing
