{- |
    this module defines Board data type
-}
module Data.Board
    ( Board
    , get
    , possibleBoardPosition
    , set
    , validBoard
    ) where
import Data.Default (Default, def)

-- | Board data type
data Board = Board [Int]

-- | insert an element between all elements in the list of elements
interleave :: a -> [a] -> [a]
interleave _ [] = []
interleave _ (x:[]) = [x]
interleave c (x:xs) = x : c : interleave c xs

-- | split a list into lists of equal amount of elements
split :: Int -> [a] -> [[a]]
split _ [] = []
split v l = take v l : split v (drop v l)

instance Show Board where
    show (Board b) = concat $ interleave "\n" $ map showLine $ split 9 b
      where
        showLine :: [Int] -> String
        showLine = interleave ' ' . concat . map show

instance Read Board where
    readsPrec _ s = [(Board $ concat $ map readLine $ lines s, "")]
      where
        readLine s' = map read $ words s'

instance Default Board where
    def = Board $ replicate (9 * 9) 0

-- | convert x-y coordinate to list index
coordToListIndex :: (Int, Int) -> Int
coordToListIndex (x, y) = x + y * 9

-- | set the number of the particular position
--
-- @
--   (0, 0)           (8, 0)
--          ----------
--         |          |
--         |          |
--         |          |
--         |          |
--          ----------
--   (0, 8)           (8, 8)
-- @
--
set :: (Int, Int) -> Int -> Board -> Board
set p v (Board b) = Board $ take index b ++ v : drop (index + 1) b
  where
    index = coordToListIndex p

-- | get the number of the particular position
--   please see `set` for the position information
get :: (Int, Int) -> Board -> Int
get p (Board b) = b !! coordToListIndex p

-- | a list of possible board position
possibleBoardPosition :: [(Int, Int)]
possibleBoardPosition = do
    x <- [0..8]
    y <- [0..8]
    return (x, y)

-- | check if a given list contains distinct value (no duplicate)
isDistinct :: (Eq a) => [a] -> Bool
isDistinct [] = True
isDistinct (x:xs) = all (/= x) xs && isDistinct xs

-- | transpose a matrix
transpose :: [[a]] -> [[a]]
transpose b
    | any isEmpty b = []
    | otherwise = map head b : transpose (map tail b)
  where
    isEmpty [] = True
    isEmpty _  = False

-- | check if a given board is valid
validBoard :: Board -> Bool
validBoard board@(Board b)
    = all isDistinct
    $ map (filter (/= 0)) (rows ++ columns ++ blocks)
  where
    rows = split 9 b
    columns = transpose rows
    blocks = map (map $ flip get board) (f <$> l <*> l)
      where
        f :: [Int] -> [Int] -> [(Int, Int)]
        f xs ys = do
            x <- xs
            y <- ys
            return (x, y)
        l :: [[Int]]
        l = split 3 [0..8]
