{- |
    this module defines implementation for BoardInput
-}
module App.State.BoardInput
    ( draw
    , handleEvent
    , attrMap
    ) where
import App.Common (AppInput(..), Direction(..), boardToWidget, posToAttr)
import App.State (BoardInput(..) , ShowAnswer(..) , GameState(..))
import Data.Board (set)
import SudokuSolver (solve)
import qualified Brick as B
import qualified Data.DoubleDirectionList as DDL
import qualified Graphics.Vty as V

-- | manual about how to play this game
manual :: B.Widget Int
manual = B.vBox
    [ B.str "ESC: quit"
    , B.str "Enter: search answer for this board"
    , B.str "Up/Down/Left/Right: move the cursor"
    , B.str "0~9: fill the number under cursor"
    ]

-- | draw function for board input state
draw :: BoardInput -> [B.Widget Int]
draw bi = [ B.vBox $ [ B.padBottom (B.Pad 1) boardW, manual ] ]
  where
    boardW = boardToWidget $ board bi

-- | next position on the board, if the given position is the last, return
--   the same value
nextPosition :: (Int, Int) -> (Int, Int)
nextPosition o@(8, 8) = o
nextPosition (x, y) = ((x + 1) `mod` 9, min 8 (y + ((x + 1) `div` 9)))

-- | handle event for board input state
handleEvent :: BoardInput -> AppInput -> [GameState]
handleEvent bi (ADirection d) =
    case d of
      DRight -> [ GSBoardInput $ bi{cursorPosition = (min 8 (x + 1), y)} ]
      DLeft  -> [ GSBoardInput $ bi{cursorPosition = (max 0 (x - 1), y)} ]
      DUp    -> [ GSBoardInput $ bi{cursorPosition = (x, max 0 (y - 1))} ]
      DDown  -> [ GSBoardInput $ bi{cursorPosition = (x, min 8 (y + 1))} ]
  where
    (x, y) = cursorPosition bi
handleEvent bi (ANumber num)
    | 0 <= num && num <= 9 = [ GSBoardInput $ bi{ board = set p num $ board bi
                                                , cursorPosition = nextP
                                                }
                             ]
    | otherwise = [ GSBoardInput bi ]
  where
    p = cursorPosition bi
    nextP = nextPosition p
handleEvent bi AEnter =
    [ GSShowAnswer $ ShowAnswer $ DDL.fromList $ solve $ board bi
    , GSBoardInput bi
    ]
handleEvent _ AEsc = []

-- | attribute map for board input state
attrMap :: BoardInput -> B.AttrMap
attrMap bi = B.attrMap V.defAttr
    [ (posToAttr $ cursorPosition bi, B.bg V.yellow)
    ]
