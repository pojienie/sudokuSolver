{- |
    this module defines App data type
-}
module App.State.ShowAnswer
    ( draw
    , handleEvent
    , attrMap
    ) where
import App.Common (AppInput(..), Direction(..), boardToWidget)
import App.State (ShowAnswer(..) , GameState(..))
import qualified Brick as B
import qualified Data.DoubleDirectionList as DDL
import qualified Graphics.Vty as V

-- | manual about how to play this game
manual :: B.Widget Int
manual = B.vBox
    [ B.str "ESC: quit"
    , B.str "Left/Right: show prev/next answer"
    ]

-- | draw function for brick
draw :: ShowAnswer -> [B.Widget Int]
draw sa = [ B.vBox [B.padBottom (B.Pad 1) boardW, manual] ]
  where
    boardW = case DDL.get $ answer sa of
        Nothing -> B.str "no answer"
        Just x -> boardToWidget x

-- | handle event for app
handleEvent :: ShowAnswer -> AppInput -> [GameState]
handleEvent sa (ADirection DLeft)
    = [ GSShowAnswer $ sa{answer = DDL.left $ answer sa} ]
handleEvent sa (ADirection DRight)
    = [ GSShowAnswer $ sa{answer = DDL.right $ answer sa} ]
handleEvent _ AEsc = []
handleEvent sa _ = [GSShowAnswer sa]

-- | attribute map for brick
attrMap :: ShowAnswer -> B.AttrMap
attrMap _ = B.attrMap V.defAttr []
