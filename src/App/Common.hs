{- |
    this module defines common data type for App
-}
module App.Common
    ( AppInput(..)
    , Direction(..)
    , boardToWidget
    , posToAttr
    ) where
import Data.Board (Board, get)
import qualified Brick as B

-- | direction
data Direction = DUp | DDown | DLeft | DRight

-- | acceptable input for app
data AppInput = AEsc | AEnter | ANumber Int | ADirection Direction

-- | convert position to attr name
posToAttr :: (Int, Int) -> B.AttrName
posToAttr (x, y) = B.attrName $ show x ++ show y

-- | draw the board
boardToWidget :: Board -> B.Widget Int
boardToWidget b = column (row block 3) 3 0 0
  where
    block x y = pad $ column (row cell 1) 1 x y
      where
        pad = B.padRight (B.Pad 1) . B.padBottom (B.Pad 1)
    row f i x y    = B.hBox $ map (\x' -> f x' y ) [x, x + i, x + 2 * i]
    column f i x y = B.vBox $ map (\y' -> f x  y') [y, y + i, y + 2 * i]
    cell x y = B.withDefAttr (posToAttr (x, y)) $ B.str $ show $ get (x, y) b
