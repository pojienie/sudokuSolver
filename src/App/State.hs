{- |
    this module defines all possible app states' data type

    why not define them in their own .hs file? Because Haskell's .hs file
    can't form import cycle.
    BoardInput.hs needs ShowAnswer and GameState
    ShowAnswer.hs needs GameState
    State.hs(this file) needs BoardInput and ShowAnswer

    Thus, I put all three data definition in here, but their implementation
    in their respective modules.
-}
module App.State
    ( BoardInput(..)
    , ShowAnswer(..)
    , GameState(..)
    ) where
import Data.Board (Board)
import Data.Default (Default, def)
import Data.DoubleDirectionList (DoubleDirectionList)
import qualified Data.DoubleDirectionList as DDL

-- | contains all data that are necessary for board input state in App
data BoardInput = BoardInput
    { board :: Board               -- ^ current board state
    , cursorPosition :: (Int, Int) -- ^ current cursor position
    }

instance Default BoardInput where
    def = BoardInput def (0, 0)

-- | contains all data that are necessary for show board state in App
data ShowAnswer = ShowAnswer
    { answer :: DoubleDirectionList Board -- ^ possible answers
    }

instance Default ShowAnswer where
    def = ShowAnswer DDL.empty

-- | possible game state
data GameState = GSBoardInput BoardInput -- ^ board input state
               | GSShowAnswer ShowAnswer -- ^ show answer state

instance Default GameState where
    def = GSBoardInput def
