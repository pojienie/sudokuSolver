{- |
    entry module
-}
module Main where
import Data.Board (Board)
import SudokuSolver (solve)
import System.Environment (getArgs, getProgName)

-- | help message
helpMessage :: String -> String
helpMessage progName = unlines
    [ "usage:"
    , progName ++ " inputFile: solve the sudoku in the input file"
    ]

-- | pretty print solutions
printSolutions :: [Board] -> IO ()
printSolutions = f 0
  where
    f :: Int -> [Board] -> IO ()
    f 0 [] = putStrLn "no solution"
    f _ [] = pure ()
    f i (x:xs) = do
        putStrLn ("solution #" ++ show i) >> print x
        f (i+1) xs

-- | entry point
main :: IO ()
main = do
    progName <- getProgName
    args <- getArgs
    case args of
        [filename] -> do
            content <- readFile filename
            printSolutions $ solve $ read content
        _ -> putStrLn $ helpMessage progName
