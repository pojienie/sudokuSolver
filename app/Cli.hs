{- |
    entry module
-}
module Main where
import App (App)
import App.Common (AppInput(..), Direction(..))
import Data.Char (ord)
import Data.Default (def)
import qualified App as App
import qualified Brick as B
import qualified Graphics.Vty as V

-- | draw function for brick
draw :: App -> [B.Widget Int]
draw = App.draw

-- | choose cursor function for brick
chooseCursor :: App -> [B.CursorLocation Int] -> Maybe (B.CursorLocation Int)
chooseCursor _ _ = Nothing

-- | handle event for brick
handleEvent :: App -> B.BrickEvent Int e -> B.EventM Int (B.Next App)
handleEvent app (B.VtyEvent (V.EvKey key _))
    | key == V.KRight = App.handleEvent app $ ADirection DRight
    | key == V.KLeft  = App.handleEvent app $ ADirection DLeft
    | key == V.KUp    = App.handleEvent app $ ADirection DUp
    | key == V.KDown  = App.handleEvent app $ ADirection DDown
    | key == V.KEnter = App.handleEvent app $ AEnter
    | key == V.KEsc   = App.handleEvent app $ AEsc
handleEvent app (B.VtyEvent (V.EvKey (V.KChar k) _))
    | 0 <= v && v <= 9 = App.handleEvent app (ANumber v)
  where
    v = ord k - ord '0'
handleEvent app _ = B.continue app

-- | start event for brick
startEvent :: App -> B.EventM Int App
startEvent = pure

-- | attribute map for brick
attrMap :: App -> B.AttrMap
attrMap = App.attrMap

-- | entry point
main :: IO ()
main = B.defaultMain brickApp def >> pure ()
  where
    brickApp = B.App draw chooseCursor handleEvent startEvent attrMap
