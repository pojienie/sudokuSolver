# sudokuSolver

This is a program that can solve sudoku problem using brute force.

# prerequisite

You'll need [stack](https://docs.haskellstack.org/en/stable/README/) to compile this program

# how to use

first pull this git repository from github:

    git clone https://github.com/PoJieNie/sudokuSolver

And use stack to compile:

	cd /your/path/to/this/repository
	stack build

After compilation, use the following command to execute:

	stack exec sudokuCmd -- [filename]
	-- or --
	stack exec sudokuCli

If you use cmd command, you need to provide a file that contains your sudoku board, the example content looks like this:

	5 3 0 0 7 0 0 0 0
	6 0 0 1 9 5 0 0 0
	0 9 8 0 0 0 0 6 0
	8 0 0 0 6 0 0 0 3
	4 0 0 8 0 3 0 0 1
	7 0 0 0 2 0 0 0 6
	0 6 0 0 0 0 2 8 0
	0 0 0 4 1 9 0 0 5
	0 0 0 0 8 0 0 7 9

Assume it is in a file called "board.txt", use the cmd command like this:

	stack exec sudokuCmd -- board.txt

And it will output all possible solution to stdout.

If you use cli command, you can fill the board interactively. here is the manual, there is a manual in the game as well:

	up/down/left/right: move cursor
	Enter: show answer
	Esc: quit
